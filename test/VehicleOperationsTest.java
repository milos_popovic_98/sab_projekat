import org.junit.Assert;
import org.junit.Test;
import rs.etf.sab.operations.GeneralOperations;
import rs.etf.sab.operations.VehicleOperations;
import rs.etf.sab.student.pm170106_GeneralOperationsImpl;
import rs.etf.sab.student.pm170106_VehicleOperationsImpl;

import java.math.BigDecimal;

public class VehicleOperationsTest {
    private GeneralOperations generalOperations;
    private VehicleOperations vehicleOperations;

    public VehicleOperationsTest() {
        generalOperations = new pm170106_GeneralOperationsImpl();
        vehicleOperations = new pm170106_VehicleOperationsImpl();
    }


    @Test
    public void insertVehicle() {
        String licencePlateNumber = "BG234DU";
        BigDecimal fuelConsumption = new BigDecimal(6.3D);
        int fuelType = 1;
        Assert.assertTrue(this.vehicleOperations.insertVehicle(licencePlateNumber, fuelType, fuelConsumption));
    }

    @Test
    public void deleteVehicles() {
        String licencePlateNumber = "BG234DU";
        BigDecimal fuelConsumption = new BigDecimal(6.3D);
        int fuelType = 1;
        this.vehicleOperations.insertVehicle(licencePlateNumber, fuelType, fuelConsumption);
        Assert.assertEquals(1L, (long) this.vehicleOperations.deleteVehicles(new String[]{licencePlateNumber}));
    }

    @Test
    public void getAllVehichles() {
        String licencePlateNumber = "BG234DU";
        BigDecimal fuelConsumption = new BigDecimal(6.3D);
        int fuelType = 1;
        this.vehicleOperations.insertVehicle(licencePlateNumber, fuelType, fuelConsumption);
        Assert.assertTrue(this.vehicleOperations.getAllVehichles().contains(licencePlateNumber));
    }

    @Test
    public void changeFuelType() {
        String licencePlateNumber = "BG234DU";
        BigDecimal fuelConsumption = new BigDecimal(6.3D);
        int fuelType = 1;
        this.vehicleOperations.insertVehicle(licencePlateNumber, fuelType, fuelConsumption);
        Assert.assertTrue(this.vehicleOperations.changeFuelType(licencePlateNumber, 2));
    }

    @Test
    public void changeConsumption() {
        String licencePlateNumber = "BG234DU";
        BigDecimal fuelConsumption = new BigDecimal(6.3D);
        int fuelType = 1;
        this.vehicleOperations.insertVehicle(licencePlateNumber, fuelType, fuelConsumption);
        Assert.assertTrue(this.vehicleOperations.changeConsumption(licencePlateNumber, new BigDecimal(7.3D)));
    }
}

