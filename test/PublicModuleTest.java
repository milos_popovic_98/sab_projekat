import org.junit.Assert;
import org.junit.Test;
import rs.etf.sab.operations.*;
import rs.etf.sab.student.*;

import java.math.BigDecimal;

public class PublicModuleTest {
    private UserOperations userOperations;
    private GeneralOperations generalOperations;
    private PackageOperations packageOperations;
    private DistrictOperations districtOperations;
    private CityOperations cityOperations;
    private VehicleOperations vehicleOperations;
    private CourierRequestOperation courierRequestOperation;
    private CourierOperations courierOperations;

    public PublicModuleTest() {
        userOperations = new pm170106_UserOperationsImpl();
        generalOperations = new pm170106_GeneralOperationsImpl();
        packageOperations = new pm170106_PackageOperationsImpl();
        districtOperations = new pm170106_DistrictOperationsImpl();
        cityOperations = new pm170106_CityOperationsImpl();
        vehicleOperations = new pm170106_VehicleOperationsImpl();
        courierRequestOperation = new pm170106_CourierRequestOperationImpl();
        courierOperations = new pm170106_CourierOperationsImpl();
    }

    @Test
    public void publicOne() {
        String courierLastName = "Ckalja";
        String courierFirstName = "Pero";
        String courierUsername = "perkan";
        String password = "sabi2018";
        userOperations.insertUser(courierUsername, courierFirstName, courierLastName, password);
        String licencePlate = "BG323WE";
        int fuelType = 0;
        BigDecimal fuelConsumption = new BigDecimal(8.3D);
        vehicleOperations.insertVehicle(licencePlate, fuelType, fuelConsumption);
        courierRequestOperation.insertCourierRequest(courierUsername, licencePlate);
        courierRequestOperation.grantRequest(courierUsername);
        Assert.assertTrue(courierOperations.getAllCouriers().contains(courierUsername));
        String senderUsername = "masa";
        String senderFirstName = "Masana";
        String senderLastName = "Leposava";
        password = "lepasampasta1";
        userOperations.insertUser(senderUsername, senderFirstName, senderLastName, password);
        int cityId = cityOperations.insertCity("Novo Milosevo", "21234");
        int cordXd1 = 10;
        int cordYd1 = 2;
        int districtIdOne = districtOperations.insertDistrict("Novo Milosevo", cityId, cordXd1, cordYd1);
        int cordXd2 = 2;
        int cordYd2 = 10;
        int districtIdTwo = districtOperations.insertDistrict("Vojinovica", cityId, cordXd2, cordYd2);
        int type1 = 0;
        BigDecimal weight1 = new BigDecimal(123);
        int packageId1 = packageOperations.insertPackage(districtIdOne, districtIdTwo, courierUsername, type1, weight1);
        BigDecimal packageOnePrice = PublicModuleTest.getPackagePrice(type1, weight1, PublicModuleTest.euclidean(cordXd1, cordYd1, cordXd2, cordYd2), new BigDecimal(5));
        int offerId = packageOperations.insertTransportOffer(courierUsername, packageId1, new BigDecimal(5));
        packageOperations.acceptAnOffer(offerId);
        int type2 = 1;
        BigDecimal weight2 = new BigDecimal(321);
        int packageId2 = packageOperations.insertPackage(districtIdTwo, districtIdOne, courierUsername, type2, weight2);
        BigDecimal packageTwoPrice =PublicModuleTest.getPackagePrice(type2, weight2, PublicModuleTest.euclidean(cordXd1, cordYd1, cordXd2, cordYd2), new BigDecimal(5));
        offerId = packageOperations.insertTransportOffer(courierUsername, packageId2, new BigDecimal(5));
       packageOperations.acceptAnOffer(offerId);
        int type3 = 1;
        BigDecimal weight3 = new BigDecimal(222);
        int packageId3 = packageOperations.insertPackage(districtIdTwo, districtIdOne, courierUsername, type3, weight3);
        BigDecimal packageThreePrice =PublicModuleTest.getPackagePrice(type3, weight3, PublicModuleTest.euclidean(cordXd1, cordYd1, cordXd2, cordYd2), new BigDecimal(5));
        offerId = packageOperations.insertTransportOffer(courierUsername, packageId3, new BigDecimal(5));
        packageOperations.acceptAnOffer(offerId);
        Assert.assertEquals(1L, (long) packageOperations.getDeliveryStatus(packageId1));
        Assert.assertEquals((long) packageId1, (long) packageOperations.driveNextPackage(courierUsername));
        Assert.assertEquals(3L, (long) packageOperations.getDeliveryStatus(packageId1));
        Assert.assertEquals(2L, (long) packageOperations.getDeliveryStatus(packageId2));
        Assert.assertEquals((long) packageId2, (long) packageOperations.driveNextPackage(courierUsername));
        Assert.assertEquals(3L, (long) packageOperations.getDeliveryStatus(packageId2));
        Assert.assertEquals(2L, (long) packageOperations.getDeliveryStatus(packageId3));
        Assert.assertEquals((long) packageId3, (long) packageOperations.driveNextPackage(courierUsername));
        Assert.assertEquals(3L, (long) packageOperations.getDeliveryStatus(packageId3));
        BigDecimal gain = packageOnePrice.add(packageTwoPrice).add(packageThreePrice);
        BigDecimal loss = (new BigDecimal(PublicModuleTest.euclidean(cordXd1, cordYd1, cordXd2, cordYd2) * 4.0D * 15.0D)).multiply(fuelConsumption);
        BigDecimal actual = courierOperations.getAverageCourierProfit(0);
        Assert.assertTrue(gain.subtract(loss).compareTo(actual.multiply(new BigDecimal(1.001D))) < 0);
        Assert.assertTrue(gain.subtract(loss).compareTo(actual.multiply(new BigDecimal(0.999D))) > 0);
    }

    static double euclidean(int x1, int y1, int x2, int y2) {
        return Math.sqrt((double)((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)));
    }

    static BigDecimal getPackagePrice(int type, BigDecimal weight, double distance, BigDecimal percentage) {
        percentage = percentage.divide(new BigDecimal(100));
        switch(type) {
            case 0:
                return (new BigDecimal(10.0D * distance)).multiply(percentage.add(new BigDecimal(1)));
            case 1:
                return (new BigDecimal((25.0D + weight.doubleValue() * 100.0D) * distance)).multiply(percentage.add(new BigDecimal(1)));
            case 2:
                return (new BigDecimal((75.0D + weight.doubleValue() * 300.0D) * distance)).multiply(percentage.add(new BigDecimal(1)));
            default:
                return null;
        }
    }
}

