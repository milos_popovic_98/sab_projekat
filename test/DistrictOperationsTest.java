import org.junit.Assert;
import org.junit.Test;
import rs.etf.sab.operations.CityOperations;
import rs.etf.sab.operations.DistrictOperations;
import rs.etf.sab.operations.GeneralOperations;
import rs.etf.sab.student.pm170106_CityOperationsImpl;
import rs.etf.sab.student.pm170106_DistrictOperationsImpl;
import rs.etf.sab.student.pm170106_GeneralOperationsImpl;

public class DistrictOperationsTest {
    private GeneralOperations generalOperations;
    private DistrictOperations districtOperations;
    private CityOperations cityOperations;
  
    public DistrictOperationsTest() {
        cityOperations = new pm170106_CityOperationsImpl();
        districtOperations = new pm170106_DistrictOperationsImpl();
        generalOperations = new pm170106_GeneralOperationsImpl();
    }

    @Test
    public void insertDistrict_ExistingCity() {
        int idCity = this.cityOperations.insertCity("Belgrade", "11000");
        Assert.assertNotEquals(-1L, (long)idCity);
        int idDistrict = this.districtOperations.insertDistrict("Palilula", idCity, 10, 10);
        Assert.assertNotEquals(-1L, (long)idDistrict);
        Assert.assertTrue(this.districtOperations.getAllDistrictsFromCity(idCity).contains(idDistrict));
    }

    @Test
    public void deleteDistricts_multiple_existing() {
        int idCity = this.cityOperations.insertCity("Belgrade", "11000");
        Assert.assertNotEquals(-1L, (long)idCity);
        String districtOneName = "Palilula";
        String districtTwoName = "Vozdovac";
        this.districtOperations.insertDistrict(districtOneName, idCity, 10, 10);
        this.districtOperations.insertDistrict(districtTwoName, idCity, 1, 10);
        Assert.assertEquals(2L, (long)this.districtOperations.deleteDistricts(new String[]{districtOneName, districtTwoName}));
    }

    @Test
    public void deleteDistrict() {
        int idCity = this.cityOperations.insertCity("Belgrade", "11000");
        Assert.assertNotEquals(-1L, (long)idCity);
        int idDistrict1 = this.districtOperations.insertDistrict("Vozdovac", idCity, 10, 10);
        Assert.assertTrue(this.districtOperations.deleteDistrict(idDistrict1));
    }

    @Test
    public void deleteAllDistrictsFromCity() {
        String cityName = "Belgrade";
        int idCity = this.cityOperations.insertCity(cityName, "11000");
        Assert.assertNotEquals(-1L, (long)idCity);
        String districtOneName = "Palilula";
        String districtTwoName = "Vozdovac";
        this.districtOperations.insertDistrict(districtOneName, idCity, 10, 10);
        this.districtOperations.insertDistrict(districtTwoName, idCity, 1, 10);
        Assert.assertEquals(2L, (long)this.districtOperations.deleteAllDistrictsFromCity(cityName));
    }

    @Test
    public void getAllDistrictsFromCity() {
        String cityName = "Belgrade";
        int idCity = this.cityOperations.insertCity(cityName, "11000");
        Assert.assertNotEquals(-1L, (long)idCity);
        String districtOneName = "Palilula";
        String districtTwoName = "Vozdovac";
        int idDistrict1 = this.districtOperations.insertDistrict(districtOneName, idCity, 10, 10);
        int idDistrict2 = this.districtOperations.insertDistrict(districtTwoName, idCity, 1, 10);
        Assert.assertTrue(this.districtOperations.getAllDistrictsFromCity(idCity).contains(idDistrict1));
        Assert.assertTrue(this.districtOperations.getAllDistrictsFromCity(idCity).contains(idDistrict2));
    }
}
