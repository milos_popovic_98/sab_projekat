CREATE PROCEDURE grantRequest
    @Username varchar(100),
    @Success int output
AS
BEGIN
	DECLARE @IdU int
	DECLARE @IdV int

    SELECT @IdU = u.IdU, @IdV = v.IdV
    FROM CourierRequest cr JOIN [User] u ON (cr.IdU = u.IdU)
        JOIN Vehicle v ON (cr.LicensePlateNumber = v.licensePlateNumber)
    WHERE Username = @Username

    IF (@IdU IS NULL OR @IdV IS NULL)
        SET @Success = 0
    ELSE
    BEGIN
        INSERT INTO Courier
        VALUES(0, 0, 0, @IdU, null)
        INSERT INTO UseVehicle(IdU, IdV)
        VALUES (@IdU, @IdV)
        DELETE FROM CourierRequest
        WHERE IdU = @IdU
        SET @Success = 1
END
    RETURN 1;
END
go