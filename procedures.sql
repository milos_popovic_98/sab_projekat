CREATE PROCEDURE grantRequest
    @Username varchar(100),
    @Success int output
AS
BEGIN
	DECLARE @IdU int
	DECLARE @IdV int

SELECT @IdU = u.IdU, @IdV = v.IdV
FROM CourierRequest cr JOIN [User] u ON (cr.IdU = u.IdU) JOIN Vehicle v ON (cr.LicensePlateNumber = v.licensePlateNumber)
WHERE Username = @Username

    IF (@IdU IS NULL OR @IdV IS NULL)
SET @Success = 0
    ELSE
BEGIN
INSERT INTO Courier
VALUES(0, 0, 0, @IdU, null)
    INSERT INTO UseVehicle(IdU, IdV)
VALUES (@IdU, @IdV)
DELETE FROM CourierRequest
WHERE IdU = @IdU
    SET @Success = 1
END
RETURN 1;
END
go

CREATE TRIGGER CourierDrive
    ON Courier
    FOR INSERT, UPDATE
                    AS
BEGIN
	DECLARE @cursor CURSOR
	DECLARE @IdU int
	DECLARE @IdV int
	DECLARE @ind int

	SET @ind = 0
	SET @cursor = CURSOR FOR
SELECT IdU
FROM inserted
WHERE Status = 1

    OPEN @cursor

	FETCH NEXT FROM @cursor
INTO @IdU

    WHILE (@@FETCH_STATUS = 0)
BEGIN
SELECT @IdV = v.IdV
FROM Vehicle v JOIN UseVehicle u ON (v.IdV = u.IdV)
WHERE u.IdU = @IdU

SELECT @ind = count(*)
FROM UseVehicle u JOIN Courier c ON (u.IdU = c.IdU)
WHERE u.IdV = @IdV AND u.IdU != @IdU AND c.Status = 1

		if (@ind > 0)
			break

		FETCH NEXT FROM @cursor
INTO @IdU
END

CLOSE @cursor
    DEALLOCATE @cursor

	if (@ind > 0)
		rollback transaction
END
go
CREATE TRIGGER updateCourierProfit
    ON Drive
    FOR UPDATE
            AS
BEGIN
	DECLARE @cursor CURSOR
	DECLARE @IdU int
	DECLARE @Profit decimal(10,3)

	SET @cursor = CURSOR FOR
SELECT IdU, Profit
FROM inserted
WHERE Status = 1

    OPEN @cursor

	FETCH NEXT FROM @cursor
INTO @IdU, @Profit

    WHILE (@@FETCH_STATUS = 0)
BEGIN
UPDATE Courier SET Profit = Profit + @Profit WHERE IdU = @IdU

    FETCH NEXT FROM @cursor
INTO @IdU, @Profit
END

CLOSE @cursor
    DEALLOCATE @cursor
END
go
CREATE TRIGGER TR_TransportOffer_AcceptOffer
    ON Package
    FOR UPDATE
            AS
BEGIN
DELETE FROM TransportOffer
WHERE IdP IN (SELECT IdP
              FROM inserted
              WHERE Status = 1)
END
go
