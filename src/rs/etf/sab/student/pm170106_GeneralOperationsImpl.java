package rs.etf.sab.student;

import rs.etf.sab.operations.GeneralOperations;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class pm170106_GeneralOperationsImpl implements GeneralOperations {

    @Override
    public void eraseAll() {
        Connection connection = DB.getInstance().getConnection();
        String[] tables = {"CourierRequest", "DrivePackage", "Drive", "TransportOffer", "UseVehicle", "Vehicle", "Package",
                "Admin", "Courier", "[User]", "District", "City",};
        for (String table : tables) {
            String deleteQuery = "DELETE FROM " + table;
            try(PreparedStatement ps = connection.prepareStatement(deleteQuery)) {
                ps.executeUpdate();
            }  catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
