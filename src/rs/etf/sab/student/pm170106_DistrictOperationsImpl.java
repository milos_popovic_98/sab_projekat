package rs.etf.sab.student;

import rs.etf.sab.operations.DistrictOperations;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class pm170106_DistrictOperationsImpl implements DistrictOperations {

    @Override
    public int insertDistrict(String name, int cityId, int xCord, int yCord) {
        Connection connection = DB.getInstance().getConnection();
        String insertQuery = "INSERT INTO District(Name, x_coord, y_coord, IdC) VALUES(?, ?, ?, ?)";
        try (PreparedStatement ps = connection.prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS)) {
            ps.setString(1, name);
            ps.setInt(2, xCord);
            ps.setInt(3, yCord);
            ps.setInt(4, cityId);
            ps.executeUpdate();
            try (ResultSet rs = ps.getGeneratedKeys()) {
                if (rs.next()) {
                    return rs.getInt(1);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public int deleteDistricts(String... names) {
        Connection connection = DB.getInstance().getConnection();
        String deleteQuery = "DELETE FROM District WHERE name = ?";
        int numberOfDeletedDistricts = 0;
        try (PreparedStatement ps = connection.prepareStatement(deleteQuery)) {
            for (String name : names) {
                ps.setString(1, name);
                numberOfDeletedDistricts += ps.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return numberOfDeletedDistricts;
    }

    @Override
    public boolean deleteDistrict(int idDistrict) {
        Connection connection = DB.getInstance().getConnection();
        String deleteQuery = "DELETE FROM District WHERE IdD = ?";
        try (PreparedStatement ps = connection.prepareStatement(deleteQuery)) {
            ps.setInt(1, idDistrict);
            return ps.executeUpdate() > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public int deleteAllDistrictsFromCity(String nameOfTheCity) {
        Connection connection = DB.getInstance().getConnection();
        String deleteQuery = "DELETE FROM District WHERE IdC IN (SELECT IdC FROM City WHERE name = ?)";
        try (PreparedStatement ps = connection.prepareStatement(deleteQuery)) {
            ps.setString(1, nameOfTheCity);
            return ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public List<Integer> getAllDistrictsFromCity(int idCity) {
        Connection connection = DB.getInstance().getConnection();
        String query = "SELECT IdD FROM District WHERE IdC = ?";
        List<Integer> allDistrictsFromCity = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, idCity);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    allDistrictsFromCity.add(rs.getInt("IdD"));
                }
                return allDistrictsFromCity;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Integer> getAllDistricts() {
        Connection connection = DB.getInstance().getConnection();
        String query = "SELECT IdD FROM District";
        List<Integer> allDistricts = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement(query);
             ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                allDistricts.add(rs.getInt("IdD"));
            }
            return allDistricts;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
