package rs.etf.sab.student;

import rs.etf.sab.operations.PackageOperations;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class pm170106_PackageOperationsImpl implements PackageOperations {

    private static final int[] PRICES = {10, 25, 75};
    private static final int[] WEIGHT_FACTORS = {0, 1, 2};
    private static final int[] PRICES_PER_KG = {0, 100, 300};

    @Override
    public int insertPackage(int districtFrom, int districtTo, String userName, int packageType, BigDecimal weight) {
        Connection connection = DB.getInstance().getConnection();
        int idU;
        String query = "SELECT IdU FROM [User] WHERE username = ?";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setString(1, userName);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    idU = rs.getInt(1);
                } else {
                    return -1;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }

        String insertQuery = "INSERT INTO Package(IdD1, IdD2, IdU, Type, Weight, Price) VALUES(?, ?, ?, ?, ?, ?)";
        try (PreparedStatement ps = connection.prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS)) {
            Double distance = calculateDistance(districtFrom, districtTo);
            BigDecimal price = calculatePrice(packageType, weight, distance);
            ps.setInt(1, districtFrom);
            ps.setInt(2, districtTo);
            ps.setInt(3, idU);
            ps.setInt(4, packageType);
            ps.setBigDecimal(5, weight);
            ps.setBigDecimal(6, price);
            ps.executeUpdate();
            try (ResultSet rs = ps.getGeneratedKeys()) {
                if (rs.next()) {
                    return rs.getInt(1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public int insertTransportOffer(String courierUserName, int packageId, BigDecimal pricePercentage) {
        if (pricePercentage == null) pricePercentage = getRandom();
        Connection connection = DB.getInstance().getConnection();
        int idC;
        String query = "SELECT c.IdU FROM Courier c JOIN [User] u ON (c.IdU = u.IdU) WHERE u.username = ? AND c.Status = 0";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setString(1, courierUserName);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    idC = rs.getInt(1);
                } else {
                    return -1;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }

        String insertQuery = "INSERT INTO TransportOffer(percentage, IdP, IdU) VALUES(?, ?, ?)";
        try (PreparedStatement ps = connection.prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS)) {
            ps.setBigDecimal(1, pricePercentage);
            ps.setInt(2, packageId);
            ps.setInt(3, idC);
            ps.executeUpdate();
            try (ResultSet rs = ps.getGeneratedKeys()) {
                if (rs.next()) {
                    return rs.getInt(1);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }


    @Override
    public boolean acceptAnOffer(int offerId) {
        Connection connection = DB.getInstance().getConnection();
        int idP, idC;
        BigDecimal percentage;
        String query = "SELECT IdP, IdU, Percentage FROM TransportOffer WHERE IdOffer = ?";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, offerId);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    idP = rs.getInt(1);
                    idC = rs.getInt(2);
                    percentage = rs.getBigDecimal(3);
                } else {
                    return false;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

        String updateQuery = "UPDATE Package SET IdC = ?, TimeAcceptance = ?, percentage = ?, Status = 1 WHERE IdP = ?";
        try (PreparedStatement ps = connection.prepareStatement(updateQuery)) {
            ps.setInt(1, idC);
            ps.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
            ps.setBigDecimal(3, percentage);
            ps.setInt(4, idP);
            return ps.executeUpdate() > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public List<Integer> getAllOffers() {
        Connection connection = DB.getInstance().getConnection();
        String query = "SELECT IdOffer FROM TransportOffer";
        List<Integer> allOffers = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    allOffers.add(rs.getInt(1));
                }
                return allOffers;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Pair<Integer, BigDecimal>> getAllOffersForPackage(int packageId) {
        Connection connection = DB.getInstance().getConnection();
        String query = "SELECT IdOffer, percentage FROM TransportOffer WHERE IdP = ?";
        List<Pair<Integer, BigDecimal>> offersForPackage = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, packageId);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    Pair<Integer, BigDecimal> pair = new pm170106_PackageOperationsPairImpl<>(rs.getInt(1), rs.getBigDecimal(2));
                    offersForPackage.add(pair);
                }
                return offersForPackage;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean deletePackage(int packageId) {
        Connection connection = DB.getInstance().getConnection();
        String deleteQuery = "DELETE FROM Package WHERE IdP = ?";
        try (PreparedStatement ps = connection.prepareStatement(deleteQuery)) {
            ps.setInt(1, packageId);
            return ps.executeUpdate() > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean changeWeight(int packageId, BigDecimal newWeight) {
        Connection connection = DB.getInstance().getConnection();
        String query = "SELECT IdD1, IdD2, Type FROM Package WHERE IdP = ?";
        BigDecimal price;
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, packageId);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    double distance = calculateDistance(rs.getInt(1), rs.getInt(2));
                    int type = rs.getInt(3);
                    price = calculatePrice(type, newWeight, distance);
                } else {
                    return false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        String updateQuery = "UPDATE Package SET Weight = ?, Price = ? WHERE IdP = ?";
        try (PreparedStatement ps = connection.prepareStatement(updateQuery)) {
            ps.setBigDecimal(1, newWeight);
            ps.setBigDecimal(2, price);
            ps.setInt(3, packageId);
            return ps.executeUpdate() > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean changeType(int packageId, int newType) {
        if (newType < 0 || newType > 2) return false;
        Connection connection = DB.getInstance().getConnection();
        String query = "SELECT IdD1, IdD2, Weight FROM Package WHERE IdP = ?";
        BigDecimal price;
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, packageId);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    double distance = calculateDistance(rs.getInt(1), rs.getInt(2));
                    BigDecimal weight = rs.getBigDecimal(3);
                    price = calculatePrice(newType, weight, distance);
                } else {
                    return false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        String updateQuery = "UPDATE Package SET Type = ?, Price = ? WHERE IdP = ?";
        try (PreparedStatement ps = connection.prepareStatement(updateQuery)) {
            ps.setInt(1, newType);
            ps.setBigDecimal(2, price);
            ps.setInt(3, packageId);
            return ps.executeUpdate() > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Integer getDeliveryStatus(int packageId) {
        Connection connection = DB.getInstance().getConnection();
        String query = "SELECT Status FROM Package WHERE IdP = ?";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, packageId);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return rs.getInt(1);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public BigDecimal getPriceOfDelivery(int packageId) {
        Connection connection = DB.getInstance().getConnection();
        String query = "SELECT Price, COALESCE(Percentage, 0) FROM Package WHERE IdP = ?";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, packageId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                BigDecimal price = rs.getBigDecimal(1);
                BigDecimal percentage = rs.getBigDecimal(2);
                percentage = percentage.divide(new BigDecimal(100));
                return price.multiply(percentage.add(new BigDecimal(1)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Date getAcceptanceTime(int packageId) {
        Connection connection = DB.getInstance().getConnection();
        String query = "SELECT TimeAcceptance FROM Package WHERE IdP = ?";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, packageId);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return new Date(rs.getTimestamp(1).getTime());
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Integer> getAllPackagesWithSpecificType(int type) {
        Connection connection = DB.getInstance().getConnection();
        String query = "SELECT IdP FROM Package WHERE Type = ?";
        List<Integer> packages = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, type);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    packages.add(rs.getInt(1));
                }
                return packages;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Integer> getAllPackages() {
        Connection connection = DB.getInstance().getConnection();
        String query = "SELECT IdP FROM Package";
        List<Integer> packages = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement(query);
             ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                packages.add(rs.getInt(1));
            }
            return packages;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Integer> getDrive(String courierUserName) {
        int currentDrive = getCurrentDrive(courierUserName);
        if (currentDrive != -1) {
            Connection connection = DB.getInstance().getConnection();
            List<Integer> packages = new ArrayList<>();
            String query = "SELECT p.IdP FROM DrivePackage dp JOIN Package p ON (dp.IdP=p.IdP) WHERE dp.IdD = ? AND p.Status <= 2";
            try (PreparedStatement ps = connection.prepareStatement(query)) {
                ps.setInt(1, currentDrive);
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        packages.add(rs.getInt(1));
                    }
                    if (!packages.isEmpty()) {
                        return packages;
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public int driveNextPackage(String courierUserName) {
        int idDrive = getCurrentDrive(courierUserName);
        if (idDrive == -1) {
            idDrive = createDrive(courierUserName);
            if (idDrive < 0) {
                return idDrive;
            }
        }
        return getDeliveredPackage(idDrive);
    }

    protected int getDeliveredPackage(int idDrive) {
        Connection connection = DB.getInstance().getConnection();
        String query = "SELECT p.IdP FROM Drive d JOIN DrivePackage dp ON (d.IdDrive = dp.IdDrive) JOIN Package p ON (dp.IdP = p.IdP)" +
                "WHERE d.IdDrive = ? AND p.Status = 2 ORDER BY p.TimeAcceptance";
        String updateQuery = "UPDATE Package SET Status = 3 WHERE IdP = ? ";
        try (PreparedStatement ps = connection.prepareStatement(query);
             PreparedStatement ps2 = connection.prepareStatement(updateQuery)) {
            ps.setMaxRows(2);
            ps.setInt(1, idDrive);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    int idP = rs.getInt(1);
                    ps2.setInt(1, idP);
                    ps2.executeUpdate();
                    calculateRouteAndUpdateLocation(idDrive, idP);
                    if (!rs.next()) finishDrive(idDrive);
                    return idP;
                } else {
                    finishDrive(idDrive);
                    return -1;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    protected void finishDrive(int idDrive) throws Exception {
        Connection connection = DB.getInstance().getConnection();
        String updateQuery = "UPDATE Drive SET Profit = ?, Status = 1 WHERE IdDrive = ?";
        try (PreparedStatement ps = connection.prepareStatement(updateQuery)) {
            BigDecimal profit = calculateProfit(idDrive);
            ps.setBigDecimal(1, profit);
            ps.setInt(2, idDrive);
            ps.executeUpdate();
        }
        String updateQuery2 = "UPDATE Courier SET Status = 0, DeliveredPackages = " +
                "DeliveredPackages +  (SELECT COUNT(*) FROM DrivePackage WHERE idDrive = ?)" +
                "WHERE idU IN (SELECT IdU FROM Drive WHERE IdDrive = ?)";
        try (PreparedStatement ps = connection.prepareStatement(updateQuery2)) {
            ps.setInt(1, idDrive);
            ps.setInt(2, idDrive);
            ps.executeUpdate();
        }
    }

    protected BigDecimal calculateProfit(int idDrive) throws Exception {
        Connection connection = DB.getInstance().getConnection();
        String query = "SELECT SUM(p.Price * (1 + p.percentage / 100)) " +
                "FROM Drive d JOIN DrivePackage dp ON (d.IdDrive = dp.IdDrive) " +
                "JOIN Package p ON (dp.IdP = p.IdP) JOIN District d1 ON (p.IdD1 = d1.IdD) " +
                "JOIN District d2 ON (p.IdD2 = d2.IdD) WHERE d.IdDrive = ?";
        String query2 = "SELECT v.FuelConsumption * f.Price FROM Drive d JOIN UseVehicle u ON (d.IdU = u.IdU) " +
                "JOIN Vehicle v ON (u.IdV = v.IdV) JOIN FuelPrice f ON (f.fuelType = v.fuelType) WHERE d.IdDrive = ?";
        String query3 = "SELECT SUM(Distance) FROM DrivePackage WHERE IdDrive = ?";
        try (PreparedStatement ps = connection.prepareStatement(query);
             PreparedStatement ps2 = connection.prepareStatement(query2);
             PreparedStatement ps3 = connection.prepareStatement(query3)) {
            ps.setInt(1, idDrive);
            ps2.setInt(1, idDrive);
            ps3.setInt(1, idDrive);
            try (ResultSet rs = ps.executeQuery();
                 ResultSet rs2 = ps2.executeQuery();
                 ResultSet rs3 = ps3.executeQuery()) {
                if (rs.next() && rs2.next() && rs3.next()) {
                    double income = rs.getDouble(1);
                    double price = rs2.getDouble(1);
                    double kilometers = rs3.getDouble(1);
                    return BigDecimal.valueOf(income - kilometers * price);
                } else {
                    throw new Exception();
                }
            }
        }


    }

    protected int getCurrentDrive(String courierUserName) {
        Connection connection = DB.getInstance().getConnection();
        String query = "SELECT d.IdDrive FROM Drive d JOIN Courier c ON (d.IdU = c.IdU) JOIN [User] u ON (c.IdU = u.IdU)" +
                "WHERE d.Status = 0 AND u.username = ?";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setString(1, courierUserName);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return rs.getInt(1);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    protected int createDrive(String courierUserName) {
        Connection connection = DB.getInstance().getConnection();
        int idC;
        //dohvatanje kurira
        String query = "SELECT c.IdU FROM Courier c JOIN [User] u ON (c.IdU = u.IdU) WHERE u.username = ?";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setString(1, courierUserName);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    idC = rs.getInt(1);
                } else {
                    return -2;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return -2;
        }

        //provera zauzetosti vozila
        query = "SELECT c.IdU FROM Courier c JOIN UseVehicle u ON (c.IdU = u.IdU) WHERE c.Status = 1 AND  c.IdU != ?" +
                " AND u.IdV IN (SELECT u.IdV FROM Courier c1 JOIN UseVehicle u1 ON (c1.IdU = u1.IdU) WHERE c1.IdU = ? ) ";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, idC);
            ps.setInt(2, idC);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return -2;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return -2;
        }
        //dohvatanje paketa za isporuku
        List<Integer> packages = new ArrayList<>();
        query = "SELECT p.IdP FROM Package p JOIN Courier c ON (p.IdC = c.IdU) WHERE p.Status = 1 AND c.IdU = ?";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, idC);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    packages.add(rs.getInt(1));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return -2;
        }
        if (packages.isEmpty()) {
            return -1;
        }

        //kreiranje voznje
        String insertQuery = "INSERT INTO Drive(IdU, Status, Profit) VALUES(?, 0, 0)";
        String insertQuery2 = "INSERT INTO DrivePackage(IdP, IdDrive) VALUES(?, ?)";
        String updateQuery = "UPDATE Courier SET Status = 1, " +
                "IdD = (SELECT IdD1 FROM Package WHERE IdP = ?) " +
                " WHERE idU = ?";
        String updateQuery2 = "UPDATE Package SET Status = 2 WHERE IdP = ?";
        try (PreparedStatement ps1 = connection.prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS);
             PreparedStatement ps2 = connection.prepareStatement(insertQuery2);
             PreparedStatement ps3 = connection.prepareStatement(updateQuery);
             PreparedStatement ps4 = connection.prepareStatement(updateQuery2)) {
            ps1.setInt(1, idC);
            ps1.executeUpdate();
            try (ResultSet generatedKey = ps1.getGeneratedKeys()) {
                if (generatedKey.next()) {
                    int idD = generatedKey.getInt(1);
                    for (int idP : packages) {
                        ps2.setInt(1, idP);
                        ps2.setInt(2, idD);
                        ps2.executeUpdate();
                        ps4.setInt(1, idP);
                        ps4.executeUpdate();
                    }
                    ps3.setInt(1, packages.get(0));
                    ps3.setInt(2, idC);
                    ps3.executeUpdate();
                    return idD;
                } else {
                    return -2;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return -2;
        }

    }

    protected BigDecimal getRandom() {
        int low = -10;
        int high = 10;
        return BigDecimal.valueOf((Math.random() * (high - low)) + low);
    }

    //CenaJedneIsporuke= (OSNOVNA_CENA[i] + (TEŽINSKI_FAKTOR[i] * weight) * CENA_PO_KG[i] ) * euklidska_distanca
    protected BigDecimal calculatePrice(int type, BigDecimal weight, Double distance) throws Exception {
        switch (type) {
            case 0:
            case 1:
            case 2:
                return (new BigDecimal((PRICES[type] + (WEIGHT_FACTORS[type] * weight.doubleValue()
                        * PRICES_PER_KG[type])) * distance));
            default:
                throw new Exception("");
        }
    }

    protected void calculateRouteAndUpdateLocation(int idDrive, int idP) throws Exception {
        Connection connection = DB.getInstance().getConnection();
        String query = "SELECT IdD1, IdD2 FROM Package WHERE IdP = ?";
        String query2 = "SELECT COALESCE(IdD, -1) FROM Courier WHERE IdU IN (SELECT IdU FROM Drive WHERE IdDrive = ?)";
        String updateQuery = "UPDATE DrivePackage SET Distance = ? WHERE IdP = ?";
        String updateQuery2 = "UPDATE Courier SET IdD = ? WHERE IdU IN (SELECT IdU FROM Drive WHERE IdDrive = ?)";
        double distance = 0;
        try (PreparedStatement ps = connection.prepareStatement(query);
             PreparedStatement ps2 = connection.prepareStatement(query2);
             PreparedStatement ps3 = connection.prepareStatement(updateQuery);
             PreparedStatement ps4 = connection.prepareStatement(updateQuery2)) {
            ps.setInt(1, idP);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    int idD1 = rs.getInt(1);
                    int idD2 = rs.getInt(2);
                    ps2.setInt(1, idDrive);
                    ResultSet rs2 = ps2.executeQuery();
                    if (rs2.next()) {
                        int idD = rs2.getInt(1);
                        if (idD != -1 && idD != idD1) {
                            distance += calculateDistance(idD, idD1);
                        }
                        distance += calculateDistance(idD1, idD2);

                        ps3.setBigDecimal(1, BigDecimal.valueOf(distance));
                        ps3.setInt(2, idP);
                        ps3.executeUpdate();

                        ps4.setInt(1, idD2);
                        ps4.setInt(2, idDrive);
                        ps4.executeUpdate();
                    }
                } else {
                    throw new Exception();
                }
            }
        }
    }

    protected Double calculateDistance(int districtFrom, int districtTo) throws Exception {
        double x1, x2, y1, y2;
        Connection connection = DB.getInstance().getConnection();
        String query = "SELECT x_coord, y_coord FROM District WHERE IdD = ?";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, districtFrom);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    x1 = rs.getBigDecimal(1).doubleValue();
                    y1 = rs.getBigDecimal(2).doubleValue();
                    ps.setInt(1, districtTo);
                    ResultSet rs1 = ps.executeQuery();
                    if (rs1.next()) {
                        x2 = rs1.getBigDecimal(1).doubleValue();
                        y2 = rs1.getBigDecimal(2).doubleValue();
                        return Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
                    }
                }
            }
        }
        throw new Exception("");
    }
}
