package rs.etf.sab.student;

import rs.etf.sab.operations.CityOperations;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class pm170106_CityOperationsImpl implements CityOperations {

    @Override
    public int insertCity(String name, String postalCode) {
        Connection connection = DB.getInstance().getConnection();
        String insertQuery = "INSERT INTO City(Name, Postal_code) VALUES(?, ?)";
        try (PreparedStatement ps = connection.prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS)) {
            ps.setString(1, name);
            ps.setString(2, postalCode);
            ps.executeUpdate();
            try (ResultSet rs = ps.getGeneratedKeys()) {
                if (rs.next()) {
                    return rs.getInt(1);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public int deleteCity(String... names) {
        Connection connection = DB.getInstance().getConnection();
        String deleteQuery = "DELETE FROM City WHERE name = ?";
        int numberOfDeletedCities = 0;
        try (PreparedStatement ps = connection.prepareStatement(deleteQuery)) {
            for (String name : names) {
                ps.setString(1, name);
                numberOfDeletedCities += ps.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return numberOfDeletedCities;
    }

    @Override
    public boolean deleteCity(int idCity) {
        Connection connection = DB.getInstance().getConnection();
        String deleteQuery = "DELETE FROM City WHERE IdC = ?";
        try (PreparedStatement ps = connection.prepareStatement(deleteQuery)) {
            ps.setInt(1, idCity);
            return ps.executeUpdate() > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public List<Integer> getAllCities() {
        Connection connection = DB.getInstance().getConnection();
        String query = "SELECT IdC FROM City";
        List<Integer> allCities = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    allCities.add(rs.getInt("IdC"));
                }
                return allCities;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
