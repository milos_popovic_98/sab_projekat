package rs.etf.sab.student;

import rs.etf.sab.operations.PackageOperations;

public class pm170106_PackageOperationsPairImpl<A, B> implements PackageOperations.Pair<A, B> {

    private transient final A firstParam;
    private transient final B secondParam;

    public pm170106_PackageOperationsPairImpl(A firstParam, B secondParam) {
        this.firstParam = firstParam;
        this.secondParam = secondParam;
    }

    @Override
    public A getFirstParam() {
        return firstParam;
    }

    @Override
    public B getSecondParam() {
        return secondParam;
    }
}
