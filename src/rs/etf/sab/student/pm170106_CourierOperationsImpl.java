package rs.etf.sab.student;

import rs.etf.sab.operations.CourierOperations;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class pm170106_CourierOperationsImpl implements CourierOperations {

    @Override
    public boolean insertCourier(String courierUserName, String licencePlateNumber) {
        Connection connection = DB.getInstance().getConnection();
        int idC, idV;
        //Check if user exists
        String queryUser = "SELECT IdU FROM [User] WHERE username = ? AND IdU NOT IN (SELECT IdU FROM Courier)";
        try (PreparedStatement ps = connection.prepareStatement(queryUser)) {
            ps.setString(1, courierUserName);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    idC = rs.getInt(1);
                } else {
                    return false;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

        //check if vehicle exists
        String queryVehicle = "SELECT IdV FROM Vehicle WHERE licensePlateNumber = ?";
        try (PreparedStatement ps = connection.prepareStatement(queryVehicle)) {
            ps.setString(1, licencePlateNumber);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    idV = rs.getInt(1);
                } else {
                    return false;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

        //insert Courier
        String insertQuery = "INSERT INTO Courier(IdU) VALUES(?)";
        try (PreparedStatement ps = connection.prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS)) {
            ps.setInt(1, idC);
            if (ps.executeUpdate() == 0)
                return false;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

        //insert Courier's vehicle
        insertQuery = "INSERT INTO UseVehicle(IdU, IdV) VALUES(?, ?)";
        try (PreparedStatement ps = connection.prepareStatement(insertQuery)) {
            ps.setInt(1, idC);
            ps.setInt(2, idV);
            return ps.executeUpdate() > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean deleteCourier(String courierUsername) {
        Connection connection = DB.getInstance().getConnection();
        String deleteQuery = "DELETE FROM Courier WHERE IdU IN (SELECT IdU FROM [User] WHERE username = ?)";
        try (PreparedStatement ps = connection.prepareStatement(deleteQuery)) {
            ps.setString(1, courierUsername);
            return ps.executeUpdate() > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public List<String> getCouriersWithStatus(int statusOfCourier) {
        if (statusOfCourier < 0 || statusOfCourier > 1) return null;
        boolean isDriving = statusOfCourier == 1;

        Connection connection = DB.getInstance().getConnection();
        String query = "SELECT u.username FROM Courier c JOIN [User] u ON (c.IdU = u.IdU) WHERE c.status = ?";
        List<String> couriers = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setBoolean(1, isDriving);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    couriers.add(rs.getString(1));
                }
                return couriers;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<String> getAllCouriers() {
        Connection connection = DB.getInstance().getConnection();
        String query = "SELECT u.username FROM Courier c JOIN [User] u ON (c.IdU = u.IdU) ORDER BY c.profit DESC";
        List<String> couriers = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    couriers.add(rs.getString(1));
                }
                return couriers;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public BigDecimal getAverageCourierProfit(int numberOfDeliveries) {
        Connection connection = DB.getInstance().getConnection();
        String avgQuery = "SELECT AVG(Profit) FROM Courier WHERE deliveredPackages >= ?";
        try (PreparedStatement ps = connection.prepareStatement(avgQuery)) {
            ps.setInt(1, numberOfDeliveries);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return rs.getBigDecimal(1);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
