package rs.etf.sab.student;

import rs.etf.sab.operations.VehicleOperations;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class pm170106_VehicleOperationsImpl implements VehicleOperations {

    @Override
    public boolean insertVehicle(String licencePlateNumber, int fuelType, BigDecimal fuelConsumption) {
        Connection connection = DB.getInstance().getConnection();
        String insertQuery = "INSERT INTO Vehicle (licensePlateNumber, fuelType, fuelConsumption) VALUES(?, ?, ?)";
        try (PreparedStatement ps = connection.prepareStatement(insertQuery)) {
            ps.setString(1, licencePlateNumber);
            ps.setInt(2, fuelType);
            ps.setBigDecimal(3, fuelConsumption);
            return ps.executeUpdate() > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public int deleteVehicles(String... licensePlateNumbers) {
        Connection connection = DB.getInstance().getConnection();
        String deleteQuery = "DELETE FROM Vehicle WHERE licensePlateNumber = ?";
        int numberOfDeletedVehicles = 0;
        try (PreparedStatement ps = connection.prepareStatement(deleteQuery)) {
            for (String licenseNumber : licensePlateNumbers) {
                ps.setString(1, licenseNumber);
                ps.executeUpdate();
                numberOfDeletedVehicles++;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return numberOfDeletedVehicles;
    }

    @Override
    public List<String> getAllVehichles() {
        Connection connection = DB.getInstance().getConnection();
        String query = "SELECT licensePlateNumber FROM Vehicle";
        List<String> allVehicles = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    allVehicles.add(rs.getString("licensePlateNumber"));
                }
                return allVehicles;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean changeFuelType(String licensePlateNumber, int fuelType) {
        Connection connection = DB.getInstance().getConnection();
        String updateQuery = "UPDATE Vehicle SET fuelType = ? WHERE licensePlateNumber = ?";
        try (PreparedStatement ps = connection.prepareStatement(updateQuery)) {
            ps.setInt(1, fuelType);
            ps.setString(2, licensePlateNumber);
            return ps.executeUpdate() > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean changeConsumption(String licensePlateNumber, BigDecimal fuelConsumption) {
        Connection connection = DB.getInstance().getConnection();
        String updateQuery = "UPDATE Vehicle SET fuelConsumption = ? WHERE licensePlateNumber = ?";
        try (PreparedStatement ps = connection.prepareStatement(updateQuery)) {
            ps.setBigDecimal(1, fuelConsumption);
            ps.setString(2, licensePlateNumber);
            return ps.executeUpdate() > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
}
