package rs.etf.sab.student;

import rs.etf.sab.operations.CourierRequestOperation;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class pm170106_CourierRequestOperationImpl implements CourierRequestOperation {

    @Override
    public boolean insertCourierRequest(String username, String licensePlateNumber) {
        Connection connection = DB.getInstance().getConnection();
        int idC;
        String query = "SELECT IdU FROM [User] WHERE username = ?";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setString(1, username);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    idC = rs.getInt(1);
                } else {
                    return false;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        String insertQuery = "INSERT INTO CourierRequest(IdU, licensePlateNumber) VALUES(?, ?)";
        try (PreparedStatement ps = connection.prepareStatement(insertQuery)) {
            ps.setInt(1, idC);
            ps.setString(2, licensePlateNumber);
            return ps.executeUpdate() > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean deleteCourierRequest(String username) {
        Connection connection = DB.getInstance().getConnection();
        String deleteQuery = "DELETE FROM CourierRequest WHERE IdU in (SELECT IdU FROM [User] WHERE username = ?)";
        try (PreparedStatement ps = connection.prepareStatement(deleteQuery)) {
            ps.setString(1, username);
            return ps.executeUpdate() > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean changeVehicleInCourierRequest(String username, String licensePlateNumber) {
        Connection connection = DB.getInstance().getConnection();
        String updateQuery = "UPDATE CourierRequest SET licensePlateNumber = ? " +
                "WHERE IdU IN (SELECT IdU FROM [User] WHERE username = ?)";
        try (PreparedStatement ps = connection.prepareStatement(updateQuery)) {
            ps.setString(1, licensePlateNumber);
            ps.setString(2, username);
            return ps.executeUpdate() > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public List<String> getAllCourierRequests() {
        Connection connection = DB.getInstance().getConnection();
        String query = "SELECT u.username FROM CourierRequest c JOIN [User] u ON (c.IdU = u.IdU)";
        List<String> requests = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    requests.add(rs.getString(1));
                }
                return requests;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean grantRequest(String username) {
        Connection conn = DB.getInstance().getConnection();
        String query = "{ call grantRequest (?,?) }";
        try (CallableStatement cs = conn.prepareCall(query)) {
            cs.setString(1, username);
            cs.registerOutParameter(2, java.sql.Types.INTEGER);
            cs.execute();
            return cs.getInt(2) == 1;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
