package rs.etf.sab.student;

import rs.etf.sab.operations.UserOperations;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class pm170106_UserOperationsImpl implements UserOperations {

    @Override
    public boolean insertUser(String username, String firstName, String lastName, String password) {
        if (checkName(firstName, lastName) && checkPassword(password)) {
            Connection connection = DB.getInstance().getConnection();
            String insertQuery = "INSERT INTO [User] (username, firstname, lastname, password, sentPackages) VALUES(?, ?, ?, ?, 0)";
            try (PreparedStatement ps = connection.prepareStatement(insertQuery)) {
                ps.setString(1, username);
                ps.setString(2, firstName);
                ps.setString(3, lastName);
                ps.setString(4, password);
                return ps.executeUpdate() > 0;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }


    @Override
    public int declareAdmin(String username) {
        Connection connection = DB.getInstance().getConnection();
        int idU;
        String queryUser = "SELECT IdU FROM [User] WHERE username = ?";
        try (PreparedStatement ps = connection.prepareStatement(queryUser)) {
            ps.setString(1, username);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    idU = rs.getInt("IdU");
                } else {
                    return 2;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return 2;
        }
        String queryAdmin = "SELECT IdU FROM Admin WHERE IdU = ?";
        try (PreparedStatement ps = connection.prepareStatement(queryAdmin)) {
            ps.setInt(1, idU);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return 1;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String insertQuery = "INSERT INTO Admin VALUES(?)";
        try (PreparedStatement ps = connection.prepareStatement(insertQuery)) {
            ps.setInt(1, idU);
            ps.executeUpdate();
            return 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public Integer getSentPackages(String... userNames) {
        Connection connection = DB.getInstance().getConnection();
        String query = "SELECT sentPackages FROM [User] WHERE username = ?";
        int numberOfSentPackages = 0;
        boolean userExists = false;
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            for (String username : userNames) {
                ps.setString(1, username);
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        userExists = true;
                        numberOfSentPackages += rs.getInt(1);
                    }
                }
            }
            if (userExists) {
                return numberOfSentPackages;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int deleteUsers(String... userNames) {
        Connection connection = DB.getInstance().getConnection();
        String deleteQuery = "DELETE FROM [User] WHERE username = ?";
        int numberOfDeletedUsers = 0;
        try (PreparedStatement ps = connection.prepareStatement(deleteQuery)) {
            for (String username : userNames) {
                ps.setString(1, username);
                numberOfDeletedUsers += ps.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return numberOfDeletedUsers;
    }

    @Override
    public List<String> getAllUsers() {
        Connection connection = DB.getInstance().getConnection();
        String query = "SELECT username FROM [User]";
        List<String> allUsers = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    allUsers.add(rs.getString(1));
                }
                return allUsers;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected boolean checkName(String firstName, String lastName) {
        return firstName != null && lastName != null
                && Character.isUpperCase(firstName.charAt(0))
                && Character.isUpperCase(lastName.charAt(0));
    }

    protected boolean checkPassword(String password) {
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-zA-z]).{8,}$";
        final Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
        final Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }
}
