package rs.etf.sab.student;

import rs.etf.sab.operations.*;
import rs.etf.sab.tests.TestHandler;
import rs.etf.sab.tests.TestRunner;

public class StudentMain {

    public static void main(String[] args) {
        CityOperations cityOperations = new pm170106_CityOperationsImpl(); // Change this to your implementation.
        DistrictOperations districtOperations = new pm170106_DistrictOperationsImpl(); // Do it for all classes.
        CourierOperations courierOperations = new pm170106_CourierOperationsImpl(); // e.g. = new MyDistrictOperations();
        CourierRequestOperation courierRequestOperation = new pm170106_CourierRequestOperationImpl();
        GeneralOperations generalOperations = new pm170106_GeneralOperationsImpl();
        UserOperations userOperations = new pm170106_UserOperationsImpl();
        VehicleOperations vehicleOperations = new pm170106_VehicleOperationsImpl();
        PackageOperations packageOperations = new pm170106_PackageOperationsImpl();
        TestHandler.createInstance(
                cityOperations,
                courierOperations,
                courierRequestOperation,
                districtOperations,
                generalOperations,
                userOperations,
                vehicleOperations,
                packageOperations);

        TestRunner.runTests();
    }
}
